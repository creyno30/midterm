#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include "ppm_io.h"
#include "imageManip.h"

void print_menu(){

  printf("\n\nHere is a list of valid commands -\n\n");

  printf("1) swap: <image_name> <new_name> swap\n");
  printf("2) grayscale: <image_name> <new_name> grayscale\n");
  printf("3) contrast: <image_name> <new_name> contrast <contrast_factor>\n");
  printf("4) zoom_in: <image_name> <new_name> zoom_in\n");
  printf("5) zoom_out: <image_name> <new_name> zoom_out\n");
  printf("6) occlude: <image_name> <new_name> occlude <top_left_x> <top_left_y> <bottom_right_x> <bottom_right_y>\n");
  printf("7) blur: <image_name> <new_name> blur <blur_factor>\n\n");
  
}

void close_shop(Image* image, FILE* output, FILE* input){

  free(image->data);
  free(image);
  fclose(output);
  fclose(input);
  
}

int main(int argc, char* argv[]){

  printf("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n\n");
  printf("           PHOTOSHOP APPLET INTERMEDIATE PROGRAMMING FALL 2018 SECTION 2\n\n");
  printf("            by Alex Ozbolt (aozbolt1) and Caroline Reynolds (creyno30)\n\n");
  printf("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n\n");
  
  if(argc < 3 || !strstr(argv[1], ".ppm") || !strstr(argv[2], ".ppm")){
    printf("Failed to supply valid input file name or output file name or both.  Be sure to use '.ppm'");
    print_menu();
    return 1;
  }
  
  FILE* fp = fopen(argv[1], "r");

  if(!fp){
    fprintf(stderr, "Input file failed to open");
    print_menu();
    return 2;
  }
  
  FILE* output = fopen(argv[2], "w");

  Image *imgp;
  imgp = read_ppm(fp);

  if(!imgp){
    fprintf(stderr, "Failed to open specified image");
    fclose(fp);
    fclose(output);
    print_menu();
    return 3;
  }
  
  if(argc < 4){
    fprintf(stderr, "No operation name specified");
    print_menu();
    close_shop(imgp, output, fp);
    return 4;
  }

  char command[strlen(argv[3]) + 1];
  strcpy(command, argv[3]);
  
  if(strcmp(command, "swap") == 0){
    if(argc != 4){
      fprintf(stderr, "Invalid number of arguments");
      print_menu();
      close_shop(imgp, output, fp);
      return 5;
    }
    swap(imgp);
  }
  else if(strcmp(command, "grayscale") == 0){
    if(argc != 4){
      fprintf(stderr, "Invalid number of arguments");
      print_menu();
      close_shop(imgp, output, fp);
      return 5;
    }
    grayscale(imgp);
  }
  else if(strcmp(command, "contrast") == 0){
    if(argc != 5){
      fprintf(stderr, "Invalid number of arguments");
      print_menu();
      close_shop(imgp, output, fp);
      return 5;
    }
    double con_fac = 1;
    
    if(sscanf(argv[4], "%lf", &con_fac) == 1){
      contrast(imgp, con_fac); 
    }
    else{
      fprintf(stderr, "Please enter a double value for contrast factor");
      print_menu();
      close_shop(imgp, output, fp);
      return 5;
    }
  }
  else if(strcmp(command, "zoom_in") == 0){
    if(argc != 4){
      fprintf(stderr, "Invalid number of arguments");
      print_menu();
      close_shop(imgp, output, fp);
      return 5;
    }
    zoom_in(imgp);
  }
  else if(strcmp(command, "zoom_out") == 0){
    if(argc != 4){
      fprintf(stderr, "Invalid number of arguments");
      print_menu();
      close_shop(imgp, output, fp);
      return 5;
    }
    zoom_out(imgp);
  }
  else if(strcmp(command, "occlude") == 0){
    if(argc != 8){
      fprintf(stderr, "Invalid number of arguments");
      print_menu();
      close_shop(imgp, output, fp);
      return 5;
    }
    int top_left_x = 0;
    int top_left_y = 0;
    int bot_right_x = 0;
    int bot_right_y = 0;
    

    if(sscanf(argv[4], "%d", &top_left_x) != 1){
      fprintf(stderr, "Please enter an integer value for top left x coordinate");
      print_menu();
      close_shop(imgp, output, fp);
      return 5;
    }	
    
    if(sscanf(argv[5], "%d", &top_left_y) != 1){
      fprintf(stderr, "Please enter an integer value for top left y coordinate");
      print_menu();
      close_shop(imgp, output, fp);
      return 5;
    }	
    
    if(sscanf(argv[6], "%d", &bot_right_x) != 1){
      fprintf(stderr, "Please enter an integer value for bottom right x coordinate");
      print_menu();
      close_shop(imgp, output, fp);
      return 5;
    }	
    
    if(sscanf(argv[7], "%d", &bot_right_y) != 1){
      fprintf(stderr, "Please enter an integer value for bottom right y coordinate");
      print_menu();
      close_shop(imgp, output, fp);
      return 5;
    }	
    
    if(occlude(imgp, top_left_x, top_left_y, bot_right_x, bot_right_y) == 0){
      print_menu();
      close_shop(imgp, output, fp);
      return 6;
    }
  }
  else if(strcmp(command, "blur") == 0){
    if(argc != 5){
      fprintf(stderr, "Invalid number of arguments");
      print_menu();
      close_shop(imgp, output, fp);
      return 5;
    }
    double blur_fac = 0;
    
    if(sscanf(argv[4], "%lf", &blur_fac) == 1){
      if(blur_fac == 0){
	fprintf(stderr, "Blur factor cannot be zero");
	print_menu();
	close_shop(imgp, output, fp);
	return 5;
      }
      blur(imgp, blur_fac);      
    }
    else{
      fprintf(stderr, "Please enter a double for the blur factor");
      print_menu();
      close_shop(imgp, output, fp);
      return 5;
    }
  }
  
  else{
    fprintf(stderr, "Invalid operation name");
    print_menu();
    close_shop(imgp, output, fp);
    return 4;
  }

  if(write_ppm(output, imgp) == 0){
    print_menu();
    close_shop(imgp, output, fp);
    return 7;
  }
  
  close_shop(imgp, output, fp);
  
  return 0;
}

