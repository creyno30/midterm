#ifndef FUNCTIONS
#define FUNCTIONS

#include <stdio.h>
#include <stdlib.h>
#include "ppm_io.h"

int swap(Image* imgptr);
int grayscale(Image* imgptr);
int contrast(Image* imgptr, double con_fac);
int zoom_in(Image* imgptr);
int zoom_out(Image *imptr);
int occlude(Image* imgptr, int up_left_x, int up_left_y, int bot_right_x, int bot_right_y);
int blur(Image* imgptr, double blur_fac);


#endif
