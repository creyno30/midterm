// ppm_io.c
// 601.220, Fall 2018
// Starter code for midterm project - feel free to edit/add to this file

#include <assert.h>
#include "ppm_io.h"
#include <stdlib.h>
#include <string.h>

/* Read a PPM-formatted image from a file (assumes fp != NULL).
 * Returns the address of the heap-allocated Image struct it
 * creates and populates with the Image data.
 */
Image * read_ppm(FILE *fp) {

  char p6[4];
  char check[1000];
  fpos_t location;
  int col =0;
  int row =0;
  int color =0;
 
  //getP6
  fscanf(fp, "%s", p6);

  fgetpos(fp, &location);
  fscanf(fp, " %s ", check);
    

  if(check[0] != '#'){ 
    fsetpos(fp, &location);
  }
  //there's a comment line
  else{
    fgets(check, 1000, fp);
  }
  
  //3 numbers separated by whitespace (columns, rows, colors)
  fscanf(fp, "%d%d%d", &col, &row, &color);
  fgetc(fp);

  if(!row || !col || color != 255 || strcmp(p6, "P6")){
    return NULL;
  }  
  //single whitespace character, after which has actual pixel values
  int num_pix = row * col;

  char *read = malloc(3 * sizeof(char) * num_pix);  
  fread(read, sizeof(char), 3 * num_pix, fp);

  Pixel *data = malloc(sizeof(Pixel)*num_pix);
  Image *to_return = malloc(sizeof(Image));

  to_return->data = data;
  to_return->rows = row;
  to_return->cols = col;


  int j = 0;
  int pixel_count = 0;
  for(int i = 0; pixel_count < num_pix; i++){
    to_return->data[i].r = read[i+j];
    j++;
    to_return->data[i].g = read[i+j];
    j++;
    to_return->data[i].b = read[i+j];
    pixel_count++;
  }

  //actual fread file

  free(read);
  if(pixel_count != num_pix){
    return NULL;
  }
  return to_return;
}


/* Write a PPM-formatted image to a file (assumes fp != NULL),
 * and return the number of pixels successfully written.
 */
int write_ppm(FILE *fp, const Image *im) {

  // check that fp is not NULL
  assert(fp); 

  // write PPM file header, in the following format
  // P6
  // cols rows
  // 255
  fprintf(fp, "P6\n%d %d\n255\n", im->cols, im->rows);

  // now write the pixel array
  int num_pixels_written = fwrite(im->data, sizeof(Pixel), im->rows * im->cols, fp);

  if (num_pixels_written != im->rows * im->cols) {
    fprintf(stderr, "Uh oh. Pixel data failed to write properly!\n");
    return 0;
  }

  return num_pixels_written;
}

