#include "imageManip.h"
#include <stdio.h>
#include <math.h>
#include "ppm_io.h"

#define PI 3.1415926535979


//swaps color channels (inverts) the image
int  swap(Image* imgptr){

   int num_pix = (imgptr->rows)*(imgptr->cols);

  for(int i = 0; i < num_pix; i++){
    char temp =  imgptr->data[i].r;
    imgptr->data[i].r = imgptr->data[i].g;
    imgptr->data[i].g = imgptr->data[i].b;
    imgptr->data[i].b = temp;
  }

  return 1;
  
}

//converts the image to grayscale
int  grayscale(Image* imgptr){

  int num_pix = (imgptr->rows)*(imgptr->cols);

  for(int i = 0; i < num_pix; i++){
    
    int intensity = 0.30*((double)imgptr->data[i].r) + 0.59*((double)imgptr->data[i].g) + 0.11*((double)imgptr->data[i].b);

    imgptr->data[i].r = intensity;
    imgptr->data[i].g = intensity;
    imgptr->data[i].b = intensity;
  }

  return 1;
  
}

//changes the contrast of the image given a factor
int contrast(Image* imgptr, double con_fac){

  int num_pix = (imgptr->rows)*(imgptr->cols);

  for(int i = 0; i < num_pix; i++){
    
    double r = (double)imgptr->data[i].r-127.5;
    double g = (double)imgptr->data[i].g-127.5;
    double b = (double)imgptr->data[i].b-127.5;

    r = (r)*con_fac;
    g = (g)*con_fac;
    b = (b)*con_fac;

    r+=127.5;
    g+=127.5;
    b+=127.5;


    if(r<0){
      r=0;
    }
    if(g<0){
      g=0;
    }
    if(b<0){
      b=0;
    }
    if(r>255){
      r=255;
    }
    if(g>255){
      g=255;
    }
    if(b>255){
      b=255;
    }

    imgptr->data[i].r = r;
    imgptr->data[i].g = g;
    imgptr->data[i].b = b;

  }

  return 1;
}

//zooms into the image
int zoom_in(Image* imgptr){

  int num_pix = 4*(imgptr->rows)*(imgptr->cols);

  Pixel* zoom_data = malloc(num_pix*sizeof(Pixel));

  int old_row = 0;
  int old_col = 0;

  for(int col = 0; col < 2*imgptr->cols; col+=2){

    for(int row = 0; row < 2*imgptr->rows; row++){

      int left = 2*row*imgptr->cols + col;
      int right = 2*(row*imgptr->cols) + col + 1;
      
      char current_r = imgptr->data[old_row*imgptr->cols + old_col].r;
      char current_g = imgptr->data[old_row*imgptr->cols + old_col].g;
      char current_b = imgptr->data[old_row*imgptr->cols + old_col].b;

      zoom_data[left].r = current_r;
      zoom_data[left].g = current_g;
      zoom_data[left].b = current_b;

      zoom_data[right].r = current_r;
      zoom_data[right].g = current_g;
      zoom_data[right].b = current_b;
      
      row++;

      left = 2*row*imgptr->cols + col;
      right = 2*(row*imgptr->cols) + col + 1;
      
      zoom_data[left].r = current_r;
      zoom_data[left].g = current_g;
      zoom_data[left].b = current_b;
      
      zoom_data[right].r = current_r;
      zoom_data[right].g = current_g;
      zoom_data[right].b = current_b;
      
      old_row++;
    }
    old_row = 0;
    old_col++;
  }
  
  free(imgptr->data);
  imgptr->data = zoom_data;
  imgptr->rows *= 2;
  imgptr->cols *= 2;
  return 1;
}

//zooms out of the image
int zoom_out(Image* imgptr){

  int num_pix = (imgptr->rows)*(imgptr->cols)/4;
  
  Pixel* zoom_data = malloc(num_pix*sizeof(Pixel));
  int pixel_count = 0;
  
  int row_difference = 0;
  if(imgptr->rows%2 == 1){
    row_difference = 1;
  }
 
  int col_difference = 0;
  if(imgptr->cols%2 == 1){
    col_difference = 1;
  }


  for(int row = 0; row < imgptr->rows - row_difference; row += 2){
    
    for(int col = 0; col < imgptr->cols - col_difference; col += 2){

      int top_left = (imgptr->cols)*row + col;
      int top_right = (imgptr->cols)*row + col + 1;
      int bot_left = top_left + imgptr->cols;
      int bot_right = top_right + imgptr->cols;
     
 
      int top_left_red = imgptr->data[top_left].r;
      int top_right_red = imgptr->data[top_right].r;
      int bot_left_red = imgptr->data[bot_left].r;
      int bot_right_red = imgptr->data[bot_right].r;

      char avg_red = (top_left_red + top_right_red + bot_left_red + bot_right_red)/4;
      zoom_data[pixel_count].r = avg_red;

      int top_left_green = imgptr->data[top_left].g;
      int top_right_green = imgptr->data[top_right].g;
      int bot_left_green = imgptr->data[bot_left].g;
      int bot_right_green = imgptr->data[bot_right].g;

      char avg_green = (top_left_green + top_right_green + bot_left_green + bot_right_green)/4;
      zoom_data[pixel_count].g = avg_green;

      int top_left_blue = imgptr->data[top_left].b;
      int top_right_blue = imgptr->data[top_right].b;
      int bot_left_blue = imgptr->data[bot_left].b;
      int bot_right_blue = imgptr->data[bot_right].b;

      char avg_blue = (top_left_blue + top_right_blue + bot_left_blue + bot_right_blue)/4;
      zoom_data[pixel_count].b = avg_blue;
      
      pixel_count++;
    }
    
  }
  free(imgptr->data);
  imgptr->data = zoom_data;
  imgptr->rows /= 2;
  imgptr->cols /= 2;

  return 1;

}
//occludes a specific area of the image
int occlude(Image* imgptr, int up_left_x, int up_left_y, int bot_right_x, int bot_right_y){

  int num_pix = imgptr->rows * imgptr->cols;
  Pixel* occ_data = calloc(num_pix, sizeof(Pixel));

  int black_on = imgptr->cols * up_left_y + up_left_x;
  int black_off = imgptr->cols * up_left_y + bot_right_x;

  if(up_left_x < 0 || up_left_y < 0 || bot_right_x >= imgptr->cols || bot_right_y >= imgptr->rows){

    fprintf(stderr, "Array indices out of bounds");
    return 0;
    
  }
  
  for(int i = 0; i < num_pix; i++){

    if(i == black_on){

      do{

	occ_data[i].r = 0;
	occ_data[i].g = 0;
	occ_data[i].b = 0;
	i++;
      }while(i <= black_off);

      if(i != imgptr->cols * bot_right_y + bot_right_x + 1){
	black_on += imgptr->cols;
	black_off += imgptr->cols;
      }
      
    }else{
      occ_data[i].r = imgptr->data[i].r;
      occ_data[i].g = imgptr->data[i].g;
      occ_data[i].b = imgptr->data[i].b;
    }   
  }

  free(imgptr->data);
  imgptr->data = occ_data;

  return 1;
  
}

//creates the gaussian  matrix given a blur factor
double** create_gauss(double sigma){

  int n = 10*sigma;
  if(n%2==0)
    n++;

  double** matrix = malloc(sizeof(double*)*n);

  for(int i=0; i<n; i++){
    matrix[i] = malloc(sizeof(double)*n);
  }

  for(int row =0; row<n; row++){
    for(int col=0; col<n; col++){

      double dx = fabs((n/2)-col);
      double dy = fabs((n/2)-row);

      matrix[row][col] = (1.0/(2.0 * PI * pow(sigma, 2))) * exp( -(pow(dx,2) + pow(dy,2)) / (2*pow(sigma,2)));

    }
  }

  return matrix;
}

//determines the number of adjacent pixels (size of gauss matrix) for a specific pixel
int find_neighbors(int row, int col, int sigma){

  int n = 10*sigma;
  if(n%2==0)
    n++;

  int neighbors = 0;
  int ld = 0;
  int rd = 0;
  int ud = 0;
  int dd = 0;
  int temp_col = col;
  int temp_row = row;

  while(temp_row-1>=0 && ud<n/2){

    ud++;
    temp_row--;
  }
  temp_row = row;

  while(temp_row+1<n && dd<n/2){

    dd++;
    temp_row++;
  }

  while(temp_col-1>=0 && ld<n/2){

    ld++;
    temp_col--;
  }
  temp_col = col;

  while(temp_col+1<n && rd<n/2){

    rd++;
    temp_col++;
  }
  

  neighbors = (ld+rd+1)*(ud+dd+1);
  return neighbors;
}

//Given a pixel in the image and the gaussian matrix, it calculates the new rgb values for the pixel
void populate_pixel(Image* img, Pixel* temp_data, int i, int neigh, double** gauss, int row, int col, int n){

  int num_cols = n;
  int pic_cols = img->cols;
  int cur_row = row;
  int cur_col = col;

  int cur_index = cur_row*img->cols + cur_col;
  
  int gauss_cen = num_cols / 2;
  int gauss_row = gauss_cen;
  int gauss_col = gauss_cen;
  
  int top_left = 1;
  int top_right = 0;
  int bot_right = 0;
  int bot_left = 0;

  double total_gauss = 0;
  double total_r = 0;
  double total_g = 0;
  double total_b = 0;

  double cur_gauss = gauss[gauss_row][gauss_col];
  
  for(int j = 0; j < neigh; j++){


    cur_index = cur_row*img->cols + cur_col;

    total_r += (double)img->data[cur_index].r * cur_gauss;
    total_g += (double)img->data[cur_index].g * cur_gauss;
    total_b += (double)img->data[cur_index].b * cur_gauss;

    total_gauss += cur_gauss;

    if(top_left){
      if (gauss_col == 0 || cur_col == 0){
	gauss_col = gauss_cen;
	cur_col = col;
	//if(gauss_row != 0 && gauss_row != up_lim){
	if(gauss_row == 0 || cur_row == 0){  
          //gauss_row--;
	  //cur_row--;
	  gauss_row = gauss_cen;
	  cur_row = row;
	  gauss_col++;
	  cur_col++;
	  top_left = 0;
	  top_right = 1;
        }else{
          gauss_row--;
	  cur_row--;
	}	
      }else{
	gauss_col--;
	cur_col--;
      }
      cur_gauss = gauss[gauss_row][gauss_col];
    }

    else if(top_right){
      
      if(gauss_col == num_cols-1 || cur_col==pic_cols-1){
	gauss_col = gauss_cen + 1;
	cur_col = col + 1;
	if(gauss_row == 0 || cur_row == 0){
	  //gauss_row--;
	  //cur_row--;
          gauss_row = gauss_cen + 1;
	  cur_row = row + 1;
          cur_col = col;
          gauss_col = gauss_cen;
	  top_right = 0;
	  bot_left = 1;

	}else{
          gauss_row--;
	  cur_row--;
	}
      }else{
	gauss_col++;
	cur_col++;
      }
    cur_gauss = gauss[gauss_row][gauss_col];
    }

    else if(bot_left){
      if(gauss_col == 0 || cur_col == 0){
	gauss_col = gauss_cen;
	cur_col = col;
	if(gauss_row == num_cols-1 || cur_row == pic_cols-1){
	  //gauss_row++;
	  //cur_row++;
	  gauss_row = gauss_cen + 1;
	  cur_row = row + 1;
	  gauss_col = gauss_cen + 1;
	  cur_col = col + 1;
	  bot_right = 1;
	  bot_left = 0;

	}else{
	  gauss_row++;
          cur_row++;   
	}
      }else{
	gauss_col--;
	cur_col--;
      }
    cur_gauss = gauss[gauss_row][gauss_col];
    }

    else if(bot_right){
      if(gauss_col == num_cols - 1 || cur_col == pic_cols-1){
	gauss_col = gauss_cen + 1;
	cur_col = col + 1;
	if(gauss_row != num_cols -1 && gauss_row != pic_cols-1){
	  gauss_row++;
	  cur_row++;
	}
      }
    }else{
      gauss_col++;
      cur_col++;
    }

  }

  int avg_r = total_r / total_gauss;
  int avg_g = total_g / total_gauss;
  int avg_b = total_b / total_gauss;

  if (fmod(avg_r, 1) >= 0.5)
    temp_data[i].r = ceil(avg_r);
  else
    temp_data[i].r = avg_r;
  if (fmod(avg_g, 1) >= 0.5)
    temp_data[i].g = ceil(avg_g);
  else
    temp_data[i].g = avg_g;
  if (fmod(avg_b, 1) >= 0.5)
    temp_data[i].b = ceil(avg_b);
  else
    temp_data[i].b = avg_b;
  
}

//Blurs the image by a specified factor 
int blur(Image* imgptr, double blur_fac){
  
  int num_pix = imgptr->rows*imgptr->cols;
  double** gauss = create_gauss(blur_fac);
  
  int row = 0;
  int col = 0;
  int num_neigh = 0;

  int n = blur_fac * 10;

  if(n%2 == 0)
    n++;
  
  Pixel* temp_data = malloc(num_pix * sizeof(Pixel));
  
  for(int i=0; i<num_pix; i++){
    
    num_neigh = find_neighbors(row, col, blur_fac);
    populate_pixel(imgptr, temp_data, i, num_neigh, gauss, row, col, n);

    col++;
    if(col==imgptr->cols){
      col=0;
      row++;
    }
  }

  free(imgptr->data);
  imgptr->data = temp_data;
  
  //free all dynamically allocated data

  for(int i=0; i<n; i++){
    free(gauss[i]);
  }
  free(gauss);

  //return blurred image
  return 1;
}
